﻿using System;
using NLog;

namespace Calculation.Interfaces
{
	public interface ILogger
	{
		void Error(Exception ex);
		void Info(string message);
		void Trace(string message);
	}

    class Log : ILogger
    {
        Logger log;
        public Log()
        {
            log = LogManager.GetCurrentClassLogger();
        }
        public void Error(Exception ex)
        {
            log.Error(ex, " - error");
        }

        public void Info(string message)
        {
            log.Info(message);
        }
        public void Trace(string message)
        {
            log.Trace(message);
        }
    }
}