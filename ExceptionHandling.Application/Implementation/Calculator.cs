﻿/*
Добавить обработку ошибок в методы класса Calculator. Требования:
1. Calculator.Sum - необходимо пробросить оригинальное исключение без изменений.
2. Calculator.Sub - необходимо пробросить оригинальное исключение, но так, чтобы его источником считался метод Sub, а не SafeSub.
3. Calculator.Multiply - нужно сгенерировать новое InvalidOperationException и сохранить оригинальное исключение внутри нового.
4. Calculator.Div - нужно сгенерировать новое InvalidOperationException, потеряв всю информацию об оригинальном исключении.

Добавить в метод Sum класса Calculator логгирование. Требования:
1. Логгируется начало выполнения метода. Сообщение должно содержать имя метода и значения параметров.
2. При успешном выполнении метода логгируется его имя, значения параметров и результат выполнения.
3. Необходимо логгировать любые возникающие исключения.
4. Независимо от результата работы метода (возвращение результата или выбрасывание исключения) логгируется 
завершение работы метода. Сообщение обязательно должно содержать имя метода, значения параметров не логгировать.
5. Уровни логгирования определить самостоятельно.

Написать собственную реализацию интерфейса Calculation.Interfaces.ILogger в проекте Calculation.
Внутри задействовать и настроить одну из библиотек - NLog, log4net, Serilog на свой выбор.
*/

using System;
using System.Linq;
using Calculation.Interfaces;
using NLog;


namespace Calculation.Implementation
{
	public class Calculator : BaseCalculator, ICalculator
	{
        Interfaces.ILogger log;
		public Calculator(Interfaces.ILogger logger)
		{
			this.log = logger;
			
		}

		//СЛОЖЕНИЕ
		public int Sum(params int[] numbers)
		{
			try
			{
				log.Trace(string.Format("Метод {0} начал работу с параметрами {1}", nameof(Sum), string.Join(" ", numbers)));
				log.Trace(string.Format($"Метод Sum завершил работу"));
				var sum = SafeSum(numbers);
				log.Info(string.Format("Метод Sum с параметрами {1} завершил работу с результатом {2}", nameof(Sum), string.Join(" ", numbers), sum));
				return sum;
			}
            catch (Exception ex)
            {
				log.Error(ex); throw;
            }

		}

		//ВЫЧИТАНИЕ
		public int Sub(int a, int b)
		{
				try
				{
					return SafeSub(a, b);
				}

				catch (Exception e)
				{
					throw e;
				}
		}
	
		//УМНОЖЕНИЕ
		public int Multiply(params int[] numbers)
		{
			try
			{
				if (!numbers.Any())
					return 0;

				return SafeMultiply(numbers);
			}
			catch (Exception e)
            {
				throw new InvalidOperationException($"Не удалось перемножить числа", e);
			}
		}

		//ДЕЛЕНИЕ
		public int Div(int a, int b)
        {
			try
			{
				return a / b;
			}
            catch
            {
				throw new InvalidOperationException($"Не удалось произвести деление");
            }
		}
	}
}